﻿using System;
using System.Linq;
using CorrelFunction;
using Moq;
using NUnit.Framework;

namespace CorrelFunctionTest
{
    [TestFixture]
    public class CorretTest
    {
        [Test]
        public void ComputeCorrelTest()
        {
            var mockCorrel = new Mock<Correl>();

            mockCorrel.Setup(x => x.Covariance(It.IsAny<float[]>(), It.IsAny<float[]>())).Returns(8);
            mockCorrel.Setup(x => x.Variance(It.IsAny<float[]>())).Returns(4);

            // Corre(a, b) = Covariance(a, b)/[(sqrl(Variance(a))*Sqrl(Variance(b))]
            //             == 8/[Sqrt(4)*Sqrt(4)] = 8/(2*2) = 2
            Assert.AreEqual(2, mockCorrel.Object.ComputeCorrel(new float[] {1, 1}, new float[] {1, 1}));
        }

        [Test]
        public void CovarianceTest_ThrowsExceptionIfNotSameNumberOfList()
        {
            float[] array1 = {2.0f, 4.0f, 8.0f};
            float[] array2 = {4.0f, 6.0f};

            Assert.Throws<Exception>(()=>new Correl().Covariance(array1, array2));
        }

        [Test]
        public void CovarianceTest()
        {
            // Covariance([2,4],[4,6]) = 2
            float[] array1 = {2.0f, 4.0f};
            float[] array2 = {4.0f, 6.0f};

            Assert.AreEqual(2f, new Correl().Covariance(array1, array2));

            float[] array3 = {3f, 6f, 8f};
            float[] array4 = {8f, 21f, 67f};
            // Covariance by definition => E(avr1 - array[i])(avr2 - array2[i])
            float array3Average = array3.Average();
            float array4Average = array4.Average();
            float expectedResult = ((array3Average - 3f)*(array4Average - 8f) +
                                    (array3Average - 6f)*(array4Average - 21f) +
                                    (array3Average - 8f)*(array4Average - 67f));
            Assert.AreEqual(expectedResult, new Correl().Covariance(array3, array4));

        }
    }
}
