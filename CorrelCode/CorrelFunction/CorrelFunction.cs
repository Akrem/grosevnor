﻿using System;
using System.Linq;

namespace CorrelFunction
{
    public class Correl
    {
        /// <summary>
        /// Referring to the link http://cameron.econ.ucdavis.edu/excel/ex51correlation.html
        /// Correl(x,y) = Convariance(x,y)/[Sqrt(Variance(x))*Sqrt(Variance(y)]
        /// </summary>
        /// <param name="floatArray1"></param>
        /// <param name="floatArray2"></param>
        /// <returns></returns>
        public float ComputeCorrel(float[] floatArray1, float[] floatArray2) {


            float sqrtVarianceArray1 = (float)Math.Sqrt(Variance(floatArray1));
            float sqrtVarianceArray2 = (float)Math.Sqrt(Variance(floatArray2));

            return Covariance(floatArray1, floatArray2) / (sqrtVarianceArray1 * sqrtVarianceArray2);
        }

        public virtual float Covariance(float[] floatArray1, float[] floatArray2) {
            if (floatArray1.Length != floatArray2.Length) {
                throw new Exception();
            }

            float covResult = 0;
            float x = floatArray1.Average();
            float y = floatArray2.Average();

            for (int i = 0; i < floatArray1.Length; i++) {
                covResult += (x - floatArray1[i]) * (y - floatArray2[i]);
            }
            return covResult;
        }

        public virtual float Variance(float[] floatArray1) {
            float varianceResult = 0;
            float x = floatArray1.Average();

            for (int i = 0; i < floatArray1.Length; i++) {
                varianceResult += (float)Math.Pow(x - floatArray1[i], 2);
            }
            return varianceResult;
        }

        private static float[] StringToFloatArray(string[] stringArray) {
            float[] floatArray = new float[stringArray.Length];
            for (int i = 0; i < stringArray.Length; i++) {
                floatArray[i] = float.Parse(stringArray[i]);
            }

            return floatArray;
        }

        static void Main(string[] args) {
            try
            {
                Correl correl = new Correl();
                Console.WriteLine("Enter the first Array, separated by space.");
                string[] firstArrayString = Console.ReadLine().Split(' ');
                float[] firstArrayFloat = StringToFloatArray(firstArrayString);

                Console.WriteLine("Enter the second Array, separated by space.");
                string[] secondArrayString = Console.ReadLine().Split(' ');
                float[] secondArrayFloat = StringToFloatArray(secondArrayString);

                float result = correl.ComputeCorrel(firstArrayFloat, secondArrayFloat);

                Console.WriteLine("The Correl of the two set is " + result);
                Console.Read();
            } catch {
                Console.WriteLine("Error: Make sure the set of numbers are entered separated with a single space." + 
                    " And the set of numbers in both set of entries should match. Please run the program again");
                Console.Read();
            }
        }
    }
}