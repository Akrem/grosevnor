﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dishes
{
    public enum DateTimeType
    {
        None = 0,
        Morning = 1,
        Night = 2
    }
    public enum FoodEntries
    {
        None = 0,
        Entrée = 1,
        Side = 2,
        Drink = 3,
        Desert = 4
    }
    public partial class WebForm1 : System.Web.UI.Page
    {
        private static readonly IDictionary<DateTimeType, string[]> FoodEntry =
            new Dictionary<DateTimeType, string[]>()
                {
                    {DateTimeType.Morning, new[] {"Eggs", "Toast", "Coffee", "Fruit"}},
                    {DateTimeType.Night, new[] {"Steak", "Potato", "Wine", "Cake"}}
                };
        

        private void Page_Load(object sender, System.EventArgs e) {
            if (!IsPostBack) {
                dbDateTime.DataSource = Enum.GetNames(typeof(DateTimeType));
                dbDateTime.DataBind();

                dbDishOne.DataSource = Enum.GetNames(typeof(FoodEntries));
                dbDishOne.DataBind();
                dbDishTwo.DataSource = Enum.GetNames(typeof(FoodEntries));
                dbDishTwo.DataBind();
                dbDishThree.DataSource = Enum.GetNames(typeof(FoodEntries));
                dbDishThree.DataBind();
                dbDishFour.DataSource = Enum.GetNames(typeof(FoodEntries));
                dbDishFour.DataBind();
            }
            lblError.Text = string.Empty;
            lblError.Visible = false;
            lblOutput.Text = string.Empty;
            lblOutput.Visible = false;
        }
        protected void btnMake_Click(object sender, EventArgs e)
        {

            string emptyValue = DateTimeType.None.ToString();
            if (dbDateTime.SelectedValue == emptyValue)
            {
                lblError.Text = "You must select Date Time.";
                lblError.Visible = true;
                return;
            }
            string dbDish1Selected = dbDishOne.SelectedValue;
            string dbDish2Selected = dbDishTwo.SelectedValue;
            string dbDish3Selected = dbDishThree.SelectedValue;
            string dbDish4Selected = dbDishFour.SelectedValue;

            if (dbDish1Selected == emptyValue && dbDish2Selected == emptyValue && dbDish3Selected == emptyValue && dbDish4Selected == emptyValue)
            {
                lblError.Text = "Atleast one dish type must be selected.";
                lblError.Visible = true;
                return;
            }

            string[] allDishes = {dbDish1Selected, dbDish2Selected, dbDish3Selected, dbDish4Selected};
            var selectedDishes = allDishes.Where(x => x != emptyValue);
            foreach (var selectedDish in selectedDishes)
            {
                string dish = selectedDish;
                if (selectedDishes.Count(x=>x==dish) > 1)
                {
                    lblError.Text = "Selected Dishes must be different.";
                    lblError.Visible = true;
                    return;
                }
            }

            string outputString = "Food to make in order: ";
            var selectedDateTime = (DateTimeType)Enum.Parse(typeof(DateTimeType), dbDateTime.SelectedValue);
            if (selectedDishes.Any(x=>x == FoodEntries.Entrée.ToString()))
            {
                outputString += FoodEntry[selectedDateTime][0] + ", ";
            }
            if (selectedDishes.Any(x=>x == FoodEntries.Side.ToString()))
            {
                outputString += FoodEntry[selectedDateTime][1] + ", ";
            }
            if (selectedDishes.Any(x=>x == FoodEntries.Drink.ToString()))
            {
                outputString += FoodEntry[selectedDateTime][2] + ", ";
            }
            if (selectedDishes.Any(x=>x == FoodEntries.Desert.ToString()))
            {
                outputString += FoodEntry[selectedDateTime][3];
            }
            lblOutput.Text = outputString;
            lblOutput.Visible = true;

        }
    }
}