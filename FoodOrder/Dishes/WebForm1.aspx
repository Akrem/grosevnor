﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Dishes.WebForm1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <p>
        <br />
        <asp:Label ID="Label1" runat="server" Text="Food Order"></asp:Label>
    </p>
    <div>
        Enter Time of Day:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="dbDateTime" runat="server">
        </asp:DropDownList>
&nbsp;&nbsp;&nbsp;
        <asp:RequiredFieldValidator ID="vTimeDay" runat="server" 
            ControlToValidate="dbDateTime" ErrorMessage="Time of Day is Required"></asp:RequiredFieldValidator>
        <br />
        <br />
        1st Dish Type Number:&nbsp;&nbsp;&nbsp;&nbsp; 
        <asp:DropDownList ID="dbDishOne" runat="server">
        </asp:DropDownList>
&nbsp;&nbsp;&nbsp;
        <br />
        2st Dish Type Number:&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="dbDishTwo" runat="server">
        </asp:DropDownList>
        <br />
        3st Dish Type Number:&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="dbDishThree" runat="server">
        </asp:DropDownList>
        <br />
        4st Dish Type Number:&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="dbDishFour" runat="server">
        </asp:DropDownList>
        <br />
        <asp:Label ID="lblError" runat="server" Visible="False"></asp:Label>
        <br />
        <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnMake" runat="server" onclick="btnMake_Click" 
            Text="Food To Make" ValidationGroup="validatorButton" />
        <br />
        <asp:Label ID="lblOutput" runat="server" Visible="False"></asp:Label>
    </div>
    </form>
</body>
</html>
