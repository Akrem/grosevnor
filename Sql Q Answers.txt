1) Write a query that returns a list of all balances for entity named �ABCD� and, for each
date shown, if available, the status on that date [defined as the �most recent up to but
not exceeding the balance date�].

ANSWER:

SELECT E.EntityName, B.BalanceDate, B.Balance, S.Status
FROM Entity AS E
	JOIN Balance AS B ON E.EntityId = B.EntityId
	LEFT JOIN Status2 AS S ON S.EntityId = E.EntityId		
WHERE S.Status = (
	SELECT Top 1 S2.Status
	FROM Balance AS B2
		JOIN Status AS S2 ON B2.EntityId = S2.EntityId
	WHERE B2.BalanceDate = B.BalanceDate AND S2.StatusDate <= B2.BalanceDate
	ORDER BY S2.StatusDate DESC
)

2) Write a query that returns average balance and most recent status for every entity

ANSWER:

SELECT E.EntityName, S.Status, S.StatusDate, AVG(B.Balance)
FROM Entity AS E
	JOIN Status AS S on E.EntityId = S.EntityId
	JOIN Balance AS B on E.EntityId = B.EntityId
WHERE S.StatusDate in (select MAX(S2.StatusDate) FROM Status S2 GROUP BY S2.EntityId)
GROUP BY E.EntityName, S.Status, S.StatusDate

